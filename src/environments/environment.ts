export const environment = {
  production: false,
  simple: 'https://api.graph.cool/simple/v1/cj7qdrzi40l1y0149bs2gp38j',
  subscription: 'wss://subscriptions.graph.cool/v1/cj7qdrzi40l1y0149bs2gp38j',
  zappier: 'https://hooks.zapier.com/hooks/catch/2322616/iujx1h/',
};
