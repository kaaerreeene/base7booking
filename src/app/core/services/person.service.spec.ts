import { TestBed, inject } from '@angular/core/testing';

import { PersonService } from './person.service';
import { Person } from '../../booking/interfaces/person.interface';

const person: Person = {
  firstName: 'firstName',
  lastName: 'lastName',
  address: {
    street: 'street',
    city: 'city',
    country: 'country',
  },
  email: 'email',
  phone: 'phone',
  notes: 'notes',
  payment: 'payment',
};

describe('PersonService', () => {
  let service: PersonService;
  let value: Person = null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PersonService],
    });
    service = TestBed.get(PersonService);
    service.person$.subscribe(res => (value = res);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be null', () => {
    expect(value).toBe(null);
  });

  it('should not be null', () => {
    service.next(person);
    expect(value).toBe(person);
  });
});
