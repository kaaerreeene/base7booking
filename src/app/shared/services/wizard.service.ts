import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

/**
 * Handles a wizard component
 */
@Injectable()
export class WizardService {
  private current = new BehaviorSubject<number>(0);
  current$ = this.current.asObservable();

  constructor() {}

  /**
   * Add one to current step
   */
  onNext(): void {
    this.setCurrent(this.getCurrent() + 1);
  }

  /**
   * Substract one to current step
   */
  onPrevious(): void {
    this.setCurrent(this.getCurrent() - 1);
  }

  /**
   * Get current step
   */
  getCurrent(): number {
    return this.current.value;
  }

  /**
   * Set current step
   * @param n number
   */
  setCurrent(n: number) {
    this.current.next(n);
  }
}
