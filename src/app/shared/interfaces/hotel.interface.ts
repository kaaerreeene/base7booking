import { Room } from './room.interface';

export interface Hotel {
  id: string;
  name: string;
  description: string;
  category: number;
  location: string;
  rooms: Room[];
  createdAt: string;
  updatedAt: string;
}
