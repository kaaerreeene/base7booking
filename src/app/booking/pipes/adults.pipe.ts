import { Pipe, PipeTransform } from '@angular/core';

/**
 * Check adults number
 */
@Pipe({
  name: 'adults',
})
export class AdultsPipe implements PipeTransform {
  /**
   * Returns adults number
   * @param rooms FormArray
   */
  transform(rooms: any[]): number {
    if (rooms) {
      return rooms
        .map(x => parseInt(x.get('adult').value))
        .reduce((prev, curr) => prev + curr);
    }

    return 0;
  }
}
