import { TotalPricePipe } from './total-price.pipe';
import { SelectedRoom } from '../interfaces/selected-room.interface';

const rooms: SelectedRoom[] = [
  {
    adult: 1,
    child: 0,
    config: {
      type: '',
      mealplan: '',
      totalPrice: 100,
    },
  },
  {
    adult: 2,
    child: 1,
    config: {
      type: '',
      mealplan: '',
      totalPrice: 99,
    },
  },
];

describe('TotalPricePipe', () => {
  let pipe: TotalPricePipe;

  beforeEach(() => {
    pipe = new TotalPricePipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return 0', () => {
    expect(pipe.transform(null)).toBe(0);
  });

  it('should return 0', () => {
    expect(pipe.transform([])).toBe(0);
  });

  it('should return 199', () => {
    expect(pipe.transform(rooms)).toBe(199);
  });
});
