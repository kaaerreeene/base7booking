import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomOptionComponent } from './components/stateless/room-option/room-option.component';
import { ReservationComponent } from './components/stateless/reservation/reservation.component';
import { ReservationRoomComponent } from './components/stateless/reservation-room/reservation-room.component';
import { ReservationBillingComponent } from './components/stateless/reservation-billing/reservation-billing.component';
import { ReservationBookingComponent } from './components/stateless/reservation-booking/reservation-booking.component';
import { ChooseDateComponent } from './components/statefull/choose-date/choose-date.component';
import { ChooseRoomComponent } from './components/statefull/choose-room/choose-room.component';
import { BookInfoComponent } from './components/statefull/book-info/book-info.component';
import { ConfirmationComponent } from './components/statefull/confirmation/confirmation.component';
import { CalendarComponent } from './components/stateless/calendar/calendar.component';
import { BookingComponent } from './components/statefull/booking/booking.component';
import { NotFoundComponent } from './components/stateless/not-found/not-found.component';
import { SharedModule } from '../shared/shared.module';
import { ReservationAlreadySelectedComponent } from './components/stateless/reservation-already-selected/reservation-already-selected.component';
import { ReservationSelectedDatesComponent } from './components/stateless/reservation-selected-dates/reservation-selected-dates.component';
import { ReservationSelectedRoomsComponent } from './components/stateless/reservation-selected-rooms/reservation-selected-rooms.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ServicesPipe } from './pipes/services.pipe';
import { MealplanPipe } from './pipes/mealplan.pipe';
import { HelpComponent } from './components/stateless/help/help.component';
import { ExtraInfoComponent } from './components/stateless/extra-info/extra-info.component';
import { NgbToDatePipe } from './pipes/ngb-to-date.pipe';
import { TotalPricePipe } from './pipes/total-price.pipe';
import { AdultsPipe } from './pipes/adults.pipe';
import { ChildrenPipe } from './pipes/children.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    SharedModule,
  ],
  declarations: [
    RoomOptionComponent,
    ReservationComponent,
    ReservationRoomComponent,
    ReservationBillingComponent,
    ReservationBookingComponent,
    ChooseDateComponent,
    ChooseRoomComponent,
    BookInfoComponent,
    ConfirmationComponent,
    CalendarComponent,
    BookingComponent,
    NotFoundComponent,
    ReservationAlreadySelectedComponent,
    ReservationSelectedDatesComponent,
    ReservationSelectedRoomsComponent,
    ServicesPipe,
    MealplanPipe,
    HelpComponent,
    ExtraInfoComponent,
    NgbToDatePipe,
    TotalPricePipe,
    AdultsPipe,
    ChildrenPipe,
  ],
  exports: [BookingComponent, NotFoundComponent],
})
export class BookingModule {}
