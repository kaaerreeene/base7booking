import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Room } from '../../../../shared/interfaces/room.interface';
import { Board } from '../../../../shared/interfaces/board.interface';

/**
 * Component for a single room option
 */
@Component({
  selector: 'b7b-room-option',
  templateUrl: './room-option.component.html',
  styleUrls: ['./room-option.component.scss'],
})
export class RoomOptionComponent {
  @Output()
  action: EventEmitter<{
    action: string;
    data: { room: Room; board: Board };
  }> = new EventEmitter();
  @Input() room: Room;

  constructor() {}

  onAddRoom(room: Room, board: Board) {
    this.action.emit({
      action: 'ADD_ROOM',
      data: {
        room: room,
        board: board,
      },
    });
  }
}
