import { Component } from '@angular/core';

/**
 * Contact information
 */
@Component({
  selector: 'b7b-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss'],
})
export class HelpComponent {
  constructor() {}
}
