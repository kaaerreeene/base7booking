import { Component } from '@angular/core';

@Component({
  selector: 'b7b-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {}
